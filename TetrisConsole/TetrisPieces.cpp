#include "stdafx.h"
#include "TetrisPieces.h"

TetrisPieces::TetrisPieces()
{
	scoreText = consoleEngine->CreateText(textX, textY, 3, scoreTextStr + "00000000", 15);
	levelText = consoleEngine->CreateText(textX, textY + 2, 3, levelTextStr + "01", 15);
	linesText = consoleEngine->CreateText(textX, textY + 4, 3, linesTextStr + "00000000", 15);

	string str = "";
	for (int w = 0; w < 10; w++) {
		str += "-";
	}
	textBackground.push_back(consoleEngine->CreateObject(textX - 1, textY - 1, 1, str, '*', 15));
	textBackground.push_back(consoleEngine->CreateObject(textX - 1, textY + 6, 1, str, '*', 15));
	str = "";
	for (int y = 0; y < 6; y++) {
		str += "|\n";
	}
	textBackground.push_back(consoleEngine->CreateObject(textX - 1, textY, 1, str, '*', 15));
	textBackground.push_back(consoleEngine->CreateObject(textX + 8, textY, 1, str, '*', 15));
	string bOut = "";
	for (auto y = 0; y < 6; y++) {
		for (auto x = 0; x < 8; x++) {
			bOut += " ";
		}
		bOut += "\n";
	}
	textBackground.push_back(consoleEngine->CreateObject(textX, textY, 1, bOut, '*', 0));


	srand((unsigned int)time(NULL));
	pieceHold = new PieceHold(board->GetX() + 36, board->GetY() + 6);
	pieceHold->SetText("   HOLD   ");
	piecePreview = new PieceHold(board->GetX() + 36, board->GetY() - 2);
	piecePreview->SetText(" PREVIEW  ");
	//Place start piece in preview
	idCounter++;
	piecePreview->PlaceInHold(new Piece(idCounter, 4, 0, board->GetX() + 1, board->GetY() + 1));
	pauseText = consoleEngine->CreateText(board->GetX() + 9, board->GetY() + 5, 2, "", 15);
	endGameText = consoleEngine->CreateText(board->GetX() + 4, board->GetY() + 5, 2, "", 15);
}

void TetrisPieces::Reset()
{
	if (activePiece != NULL) {
		activePiece->DeleteSquares();
		delete(activePiece);
		activePiece = NULL;
	}
	for (auto &p : placedPieces) {
		p->DeleteSquares();
	}
	placedPieces.clear();
	pieceHold->RemoveHold();
	//piecePreview->RemoveHold();
	piecePreview->PlaceInHold(new Piece(idCounter, 4, 0, board->GetX() + 1, board->GetY() + 1));
	speed = 2.0f;
	timerCounter = 0.0f;
	idCounter = 0;
	moved = 0;
	score = 0;
	level = 1;
	lineTotal = 0;
	endGameText->ChangeText("");
	endGame = false;
	scoreText->ChangeText(scoreTextStr + "00000000");
	levelText->ChangeText(levelTextStr + "01");
	linesText->ChangeText(linesTextStr + "00000000");
}

void TetrisPieces::GeneratePiece()
{
	timerCounter = 0.0f;
	moved = 0;
	idCounter++;
	activePiece = new Piece(idCounter, 4, 0, board->GetX() + 1, board->GetY() + 1, 0, piecePreview->GetHoldPiece()->GetPiece());
	piecePreview->PlaceInHold(new Piece(idCounter, 4, 0, board->GetX() + 1, board->GetY() + 1));
	int xPixel = activePiece->GetBoardX();
	int yPixel = activePiece->GetBoardY();
	UpdateBoardLocation(activePiece);
}

void TetrisPieces::GeneratePiece(int piece, int rot)
{
	timerCounter = 0.0f;
	moved = 0;
	idCounter++;
	activePiece = new Piece(idCounter, 4, 0, board->GetX() + 1, board->GetY() + 1, rot, piece);
	int xPixel = activePiece->GetBoardX();
	int yPixel = activePiece->GetBoardY();
	UpdateBoardLocation(activePiece);
}

bool TetrisPieces::IfActivePiece()
{
	return activePiece == NULL;
}

bool TetrisPieces::Update(float timer)
{
	//Get input
	int in = consoleEngine->GetInput();
	if (!paused && !endGame) {
		timerCounter += speed * timer;
		if (timerCounter > 1) {
			timerCounter = 0.0f;
			if (!BottomCollision()) {
				activePiece->MoveDown(1);
				UpdateBoardLocation(activePiece);
				moved++;
			}
			else {
				if (moved == 0) {
					string str = "";
					for (size_t a = 0; a < 8 - to_string(score).size(); a++) {
						str += "0";
					}
					str += to_string(score);
					endGameText->ChangeText(" Score:" + str + "\nEscape to reset!");
					endGame = true;
				}
				RemoveActivePiece();
				return false;
			}
		}

		//Deal with inputs
		if (in == 100 && activePiece->GetBoardX() + activePiece->GetWidth() < board->GetWidth() && !SideCollision(1)) { //D
			{
				activePiece->MoveRight(1);
				UpdateBoardLocation(activePiece);
			}
		}
		else if (in == 97 && activePiece->GetBoardX() > 0 && !SideCollision(-1)) { //A
			activePiece->MoveLeft(1);
			UpdateBoardLocation(activePiece);
		}
		else if (in == 115) { //S
			while (!BottomCollision()) {
				activePiece->MoveDown(1);
				UpdateBoardLocation(activePiece);
				moved++;
			}
		}
		else if (in == 101) { //E
			ObjectOnBoard(activePiece, false);
			activePiece->RotateLeft();
			UpdateBoardLocation(activePiece);
		}
		else if (in == 113) { //Q
			ObjectOnBoard(activePiece, false);
			activePiece->RotateRight();
			UpdateBoardLocation(activePiece);
		}
		else if (in == 32 && pieceHold->GetAllowed()) { //Space
			for (auto &square : activePiece->squares) {
				board->ChangeTaken(square->boardX, square->boardY, false);
			}
			if (!pieceHold->IfEmpty()) {
				int rot = pieceHold->GetHoldPiece()->GetRotation();
				int pieceNum = pieceHold->GetHoldPiece()->GetPiece();
				pieceHold->PlaceInHold(activePiece);
				GeneratePiece(pieceNum, rot);
			}
			else {
				pieceHold->PlaceInHold(activePiece);
				GeneratePiece();
			}
			pieceHold->SetAllowed(false);
			return false;
		}
	}
	
	//Escape can be pressed at any point
	if (in == 27) { //Escape
		if (!endGame) {
			if (paused) {
				paused = false;
				pauseText->ChangeText("");
			}
			else if (!paused) {
				paused = true;
				pauseText->ChangeText("PAUSED");
			}
		}
		else if (endGame) {
			return true;
		}
	}
	return false;
}

bool TetrisPieces::IfCollision(int xPixel, int yPixel)
{
	return board->GetTaken(xPixel, yPixel);
}

void TetrisPieces::UpdateBoardLocation(Piece* p)
{
	//Clear old pos from board tracker
	ObjectOnBoard(p, false);
	p->lastLocations.clear();
	//Set new location on board tracker
	for (auto &s : p->squares) {
		if (s->piece != NULL && s->boardY < board->GetHeight()) {
			board->ChangeTaken(s->boardX, s->boardY, true);
			p->lastLocations.push_back(make_pair(s->boardX, s->boardY));
		}
	}
}

bool TetrisPieces::SideCollision(int side)
{
	ObjectOnBoard(activePiece, false);
	//Set new location on board tracker
	for (auto &s : activePiece->squares) {
		if (board->GetTaken(s->boardX + side, s->boardY)) {
			ObjectOnBoard(activePiece, true);
			return true;
		}
	}
	ObjectOnBoard(activePiece, true);
	return false;
}

bool TetrisPieces::BottomCollision()
{
	if (activePiece->GetBoardY() + activePiece->GetHeight() == board->GetHeight()) {
		return true;
	}
	ObjectOnBoard(activePiece, false);
	//Set new location on board tracker
	for (auto &s : activePiece->squares) {
		if (board->GetTaken(s->boardX, s->boardY + 1)) {
			ObjectOnBoard(activePiece, true);
			return true;
		}
	}
	ObjectOnBoard(activePiece, true);
	return false;
}

void TetrisPieces::ObjectOnBoard(Piece* p, bool b)
{
	for (auto &s : p->lastLocations) {
		board->ChangeTaken( s.first, s.second, b);
	}
}

void TetrisPieces::RemoveActivePiece()
{
	pieceHold->SetAllowed(true);
	placedPieces.push_back(activePiece);
	activePiece = NULL;
}

void TetrisPieces::RemoveLine(vector<int> lines)
{
	//Remove lines
	for (auto &y : lines) {
		for (auto &obj : placedPieces) {
			for (size_t a = 0; a < obj->squares.size(); a++) {
				if (obj->squares[a]->boardY == y && obj->squares[a]->piece != NULL) {
					consoleEngine->DeleteObject(obj->squares[a]->piece);
					obj->squares[a]->piece = NULL;
					board->ChangeTaken(obj->squares[a]->boardX, y, false);
				}
			}
		}
	}

	//Move other lines down
	sort(lines.begin(), lines.end());
	for (auto &l : lines) {
		for (auto &p : placedPieces) {
			p->MoveDown(1, l);
			UpdateBoardLocation(p);
		}
	}

	//Update stats
	lineTotal += lines.size();
	if (level < 20 && lineTotal > level * 10) {
		level++;
		speed++;
	}
	score += scoreTable[lines.size() - 1] * (level + 1);

	//Update score text
	string str = "";
	for (size_t a = 0; a < 8 - to_string(score).size(); a++) {
		str += "0";
	}
	scoreText->ChangeText(scoreTextStr + str + to_string(score));
	//Update level text
	str = "";
	for (size_t a = 0; a < 2 - to_string(level).size(); a++) {
		str += "0";
	}
	levelText->ChangeText(levelTextStr + str + to_string(level));
	//Update lines text
	str = "";
	for (size_t a = 0; a < 8 - to_string(lineTotal).size(); a++) {
		str += "0";
	}
	linesText->ChangeText(linesTextStr + str + to_string(lineTotal));
}

int TetrisPieces::GetScore()
{
	return score;
}
