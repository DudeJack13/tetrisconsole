#pragma once
#ifndef LIVELOGGER_H
#define LIVELOGGER_H
#include <string>
#include <fstream>


#define DEBUG


using namespace std;

class LiveLogger
{
private:
	string dir = "";
	string wholeDir = "";
	void _SendMessage(string str);
public:
	LiveLogger() {}
	LiveLogger(string str);
	~LiveLogger() { logInfo(wholeDir + " is no longer logging"); };

	void logInfo(string str) { _SendMessage("INFO: " + str); }
	void logWarning(string str) { _SendMessage("WARNING: " + str); }
	void logError(string str) { _SendMessage("ERROR: " + str); }
};
#endif