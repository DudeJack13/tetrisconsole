#include "stdafx.h"
#include "MenuStars.h"

MenuStars::MenuStars(int id, int width, int height)
{
	srand((unsigned int)time(NULL) + id);
	this->colour = rand() % 8;
	this->brightColour = colour + 8;
	this->speed = (float)(rand() % 5);
	star = consoleEngine->CreateObject(rand() % width, rand() % height, 0, "*", 'X', colour);
}

MenuStars::~MenuStars()
{
	consoleEngine->DeleteObject(star);
}

void MenuStars::Update(float timer)
{
	counter += speed * timer;
	if (counter > 1.0f) {
		counter = 0.0f;
		state++;
		if (state == 1) {
			star->SetColour(brightColour);
		}
		else if (state == 2) {
			star->SetColour(colour);
		}
		else {
			dead = true;
		}
	}
}

bool MenuStars::GetDead()
{
	return dead;
}
