#include "stdafx.h"
#include "PieceHold.h"


PieceHold::PieceHold(int boardX, int boardY)
{
	string str = "";
	for (int w = 0; w < 10; w++) {
		str += "-";
	}
	holdX = boardX - str.size() - 1;
	holdY = boardY + 3;
	objLocationX = holdX + 1;
	objLocationY = holdY + 1;
	lines.push_back(consoleEngine->CreateObject(holdX, holdY, 2, str, '*', 15));
	lines.push_back(consoleEngine->CreateObject(holdX, holdY + 5, 2, str, '*', 15));
	str = "";
	for (int h = 0; h < 4; h++) {
		str += "|\n";
	}
	lines.push_back(consoleEngine->CreateObject(holdX, holdY + 1, 2, str, '*', 15));
	lines.push_back(consoleEngine->CreateObject(holdX + 9, holdY + 1, 2, str, '*', 15));
	header = consoleEngine->CreateText(holdX, holdY - 1, 0, "", 15);

	string bOut = "";
	for (int y = 0; y < 6; y++) {
		for (int x = 0; x < 10; x++) {
			bOut += " ";
		}
		bOut += "\n";
	}
	background = consoleEngine->CreateObject(holdX, holdY, 1, bOut, '*', 0);
}

void PieceHold::PlaceInHold(Piece * _piece)
{
	if (piece != NULL) {
		piece->DeleteSquares();
		delete(piece);
	}
	this->piece = _piece;
	this->piece->SetWorldX(objLocationX);
	this->piece->SetWorldY(objLocationY);
}

Piece * PieceHold::GetHoldPiece()
{
	return piece;
}

bool PieceHold::IfEmpty()
{
	return piece == NULL;
}

void PieceHold::SetAllowed(bool b)
{
	allowed = b;
}

bool PieceHold::GetAllowed()
{
	return allowed;
}

void PieceHold::SetText(string str)
{
	text = str;
	header->ChangeText(text);
}

void PieceHold::RemoveHold()
{
	if (piece != NULL) {
		piece->DeleteSquares();
	}
}