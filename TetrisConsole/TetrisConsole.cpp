// TetrisConsole.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include "ConsoleEngine.h"
#include "Board.h"
#include "TetrisPieces.h"
#include "Logger\LiveLogger.h"
#include "MainMenu.h"

#include <conio.h> 
#include <stdio.h>

ConsoleEngine* consoleEngine = NULL;
Board* board = NULL;
LiveLogger* liveLogger = NULL;

using namespace consoleengine;

int main(int argc, char* argv[])
{
	//-----------------------------
	//Set up Engine
	//-----------------------------
	liveLogger = new LiveLogger(argv[0]);
	consoleEngine = new ConsoleEngine(350, 555);
	consoleEngine->SetBounds(38,31);
	liveLogger->logInfo("Console Engine Started");

	int state = 0;

	//-----------------------------
	//Set up main menu
	//-----------------------------
	MainMenu* mainMenu = new MainMenu(consoleEngine->GetBoundsWidth(), consoleEngine->GetBoundsHeight());

	//-----------------------------
	//Set up game
	//-----------------------------
	//Load Tetris pieces
	TetrisPieces* tetrisPieces = NULL;

	//-----------------------------
	//Game loop
	//-----------------------------
	liveLogger->logInfo("Loop Starting");
	while (consoleEngine->Running()) {
		float timer = consoleEngine->Timer();
		if (state == 0) {
			int returnInt = mainMenu->Update(timer);
			if (returnInt == 1) {
				mainMenu->~MainMenu();
				board = new Board(1, 1, DEFAULT_BOARD_WIDTH, DEFAULT_BOARD_HEIGHT);
				tetrisPieces = new TetrisPieces();
				liveLogger->logInfo("Board and Tetris Pieces Created");
				state = 1;
			}
			else if (returnInt == 2) {
				consoleEngine->ExitEngine();
			}
		}
		else if (state == 1) {
			if (tetrisPieces->IfActivePiece()) {
				//Check board lines
				vector<int> lines = board->CheckLines();
				if (!lines.empty()) {
					liveLogger->logInfo("Lines Amount:" + to_string(lines.size()));
					tetrisPieces->RemoveLine(lines);
				}
				tetrisPieces->GeneratePiece();
			}
			if (tetrisPieces->Update(timer)) { //End game
				board->Reset();
				tetrisPieces->Reset();
			}
		}
	}

	//-----------------------------
	//Unload
	//-----------------------------
	liveLogger->~LiveLogger();
    return 0;
}

