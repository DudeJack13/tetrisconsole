#pragma once
#ifndef MENUSTARS_H
#define MENUSTARS_H

#include <stdlib.h>
#include <time.h>

#include "ConsoleEngine.h"
#include "MenuStars.h"

using namespace consoleengine;
extern ConsoleEngine* consoleEngine;

class MenuStars
{
private:
	Object * star = NULL;
	float speed = 2.0f;
	float counter = 0.0f;
	int state = 0;
	int colour = 0;
	int brightColour = 0;
	bool dead = false;
public:
	MenuStars() {}
	MenuStars(int id, int width, int height);
	~MenuStars();

	void Update(float timer);
	bool GetDead();
};

#endif // !MENUSTARS_H