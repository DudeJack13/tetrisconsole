#pragma once
#ifndef PIECE_H
#define PIECE_H

#include <stdlib.h>
#include <time.h>

#include "ConsoleEngine.h"
#include "Board.h"
#include "TetrisPiecesPatterns.h"

using namespace consoleengine;

extern ConsoleEngine* consoleEngine;
extern Board* board;

class Piece
{
private:
	struct Square
	{
		Square(Object* obj, int boardX, int boardY) {
			this->boardX = boardX;
			this->boardY = boardY;
			piece = obj;
		}
		int boardX;
		int boardY;
		Object* piece;
	};

	int boardX;
	int boardY;
	int worldX;
	int worldY;
	int currentPiece = -1;
	int rotateNum = 0;
	int width = 0;
	int height = 0;
	int randomCounter = 0;

	void CreateSquares(string str, int colour);
public:
	Piece() {}
	Piece(int id, int x, int y, int boardXLocation, int boardYLocation);
	Piece(int id, int x, int y, int boardXLocation, int boardYLocation, int rot, int piece);
	~Piece() {}

	void DeleteSquares();

	vector <Square*> squares;
	vector<pair<int, int>> lastLocations;

	void MoveLeft(int amount);
	void MoveRight(int amount);
	void MoveDown(int amount);
	void MoveDown(int amount, int y);

	int GetBoardX();
	int GetBoardY();
	void SetWorldX(int x);
	void SetWorldY(int y);
	int GetHeight();
	int GetWidth();

	void RotateLeft();
	void RotateRight();
	int GetRotation();

	int GetPiece();
};

#endif // !PIECE_H