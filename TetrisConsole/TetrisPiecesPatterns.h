#pragma once
#ifndef TETRISPIECESPATTERNS_H
#define TETRISPIECESPATTERNS_H

#include <string>
#include <vector>

using namespace std;

enum Pieces
{
	I, J, L, O, S, T, Z
};

const int PIECESAMOUNT = 7;
const char TRANSPARENTCHAR = '*';

struct SPiece
{
	SPiece(vector <string> vstr, int colour) {
		this->piece = vstr;
		this->colour = colour;
	}
	vector <string> piece;
	int colour;
};

const vector <SPiece*> PIECES = {
	new SPiece(vector<string> {
		"[][][][]",
		"[]\n[]\n[]\n[]"
	}, 159),

	new SPiece(vector<string> {
		"[][][]\n****[]",
		"**[]\n**[]\n[][]",
		"[]******\n[][][]",
		"[][]\n[]**\n[]**"
	}, 31),

	new SPiece(vector<string> {
		"[][][]\n[]****",
		"[][]\n**[]\n**[]",
		"****[]\n[][][]",
		"[]**\n[]**\n[][]"
	}, 223),

	new SPiece(vector<string> {
		"[][]\n[][]",
	}, 239),

	new SPiece(vector<string> {
		"**[][]\n[][]**",
		"[]**\n[][]\n**[]",
	}, 175),

	new SPiece(vector<string> {
		"[][][]\n**[]**",
		"**[]\n[][]\n**[]",
		"**[]**\n[][][]",
		"[]**\n[][]\n[]**"
	}, 95),

	new SPiece(vector<string> {
		"[][]**\n**[][]",
		"**[]\n[][]\n[]**",
	}, 207),
};

#endif // !TETRISPIECESPATTERNS_H