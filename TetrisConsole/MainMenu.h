﻿#pragma once
#ifndef MAINMENU_H
#define MAINMENU_H

#include "ConsoleEngine.h"
#include "MenuStars.h"

using namespace consoleengine;
extern ConsoleEngine* consoleEngine;

class MainMenu
{
private:
	int width = 0;
	int height = 0;
	Text* header = NULL;
	const string tH1 = "  _______   _        _";
	const string tH2 = " |__   __| | |      (_)";
	const string tH3 = "    | | ___| |_ _ __ _ ___";
	const string tH4 = "    | |/ _ \\ __| '__| / __|";
	const string tH5 = "    | |  __/ |_| |  | \\__ \\";
	const string tH6 = "    |_|\\___|\\__|_|  |_|___/";
	const string tetrisHeader = tH1 + "\n" + tH2 + "\n" + tH3 + "\n" + tH4 + "\n" + tH5 + "\n" + tH6;
	const vector <string> optionText = { "Play", "Quit" };
	const string arrowStr = "--->      <---";
	const string version = "v1.0";
	Text* optionsText = NULL;
	Text* arrow = NULL;
	Text* versionText = NULL;
	int currentSelected = 0;
	Object* backgroundBox = NULL;
	Object* backgroundBoxBoarder = NULL;
	const int yOffset = 2;
	int idCounter = 0;
	vector<MenuStars*> stars;
public:
	MainMenu() {}
	MainMenu(int width, int height);
	~MainMenu();

	int Update(float frametime);
};

#endif // !MAINMENU_H