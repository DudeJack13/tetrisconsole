#pragma once
#ifndef BOARD_H
#define BOARD_H

#include <string>
#include "ConsoleEngine.h"

using namespace consoleengine;
extern ConsoleEngine* consoleEngine;

const int DEFAULT_BOARD_WIDTH = 11;
const int DEFAULT_BOARD_HEIGHT = 26;

class Board
{
protected:
	struct BoardSquare
	{
		BoardSquare(int x, int y) {
			this->x = x;
			this->y = y;
		}
		bool taken = false;
		int x = -1;
		int y = -1;
	};
	int boardX = 0;
	int boardY = 0;
	vector<BoardSquare*> board;
	int cellWidth = 0;
	int cellHeight = 0;
	int boardHeight = 0;
	int boardWidth = 0;
	const char horizontalChar = '-';
	const char verticalChar = '|';
	vector<Object*> boarderLines;

	Object* backgroundSquare = NULL;
	Object* boardBackground = NULL;

	Text* authorName = NULL;

	void CreateBoard(int boardWidth, int boardHeight);

	//Test print
	int textboardX = 30;

public:
	//Use default board size
	Board();
	//Enter custom board size
	Board(int x, int y, int cellWidth, int cellHeight);
	~Board() {}

	void Reset();
	vector<int> CheckLines();

	//Check to see if piece has hit something
	bool HitBottom(Object* obj);

	void ChangeTaken(int x, int y, bool taken);
	bool GetTaken(int x, int y);

	//X and Y Getters
	int GetX();
	int GetY();
	void ConvertToBoardPosition(int &x, int &y);

	//Get Width and height
	int GetWidth();
	int GetHeight();
};

#endif // !BOARD_H