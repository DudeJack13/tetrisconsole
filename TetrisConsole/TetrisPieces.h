#pragma once
#ifndef TETRISPIECES
#define TETRISPIECES

#include <string>
#include <vector>
#include <stdlib.h>
#include <time.h>
#include <algorithm>
#include <iomanip>

#include "ConsoleEngine.h"
#include "Board.h"
#include "Logger\LiveLogger.h"
#include "Piece.h"
#include "PieceHold.h"

using namespace consoleengine;

extern ConsoleEngine* consoleEngine;
extern Board* board;
extern LiveLogger* liveLogger;

class TetrisPieces
{
private:
	PieceHold* pieceHold = NULL;
	PieceHold* piecePreview = NULL;

	Piece* activePiece = NULL;
	vector<Piece*> placedPieces;

	float speed = 2.0f;
	float timerCounter = 0.0f;
	int idCounter = 0;

	int moved = 0;

	Text* scoreText = NULL;
	Text* levelText = NULL;
	Text* linesText = NULL;
	const string scoreTextStr = " Score  \n";
	const string levelTextStr = " Level  \n";
	const string linesTextStr = " Lines  \n";
	const int textX = board->GetX() + 26;
	const int textY = 18;
	vector<Object*> textBackground;
	int score = 0;
	int level = 1;
	int lineTotal = 0;
	const int scoreTable[4] = {
		40,
		100,
		300,
		1200
	};

	bool endGame = false;
	Text* endGameText = NULL;
	bool paused = false;
	Text* pauseText = NULL;

	void UpdateBoardLocation(Piece* p);
	void RemoveActivePiece();

	//Collision
	bool BottomCollision();
	bool SideCollision(int side);

	bool IfCollision(int xPixel, int yPixel);
	void ObjectOnBoard(Piece* p, bool b);
public:
	TetrisPieces();
	~TetrisPieces() {}

	void GeneratePiece();
	void GeneratePiece(int piece, int rot);
	bool IfActivePiece();
	bool Update(float timer);
	void RemoveLine(vector<int> lines);
	int GetScore();
	void Reset();
};

#endif // !TETRISPIECES