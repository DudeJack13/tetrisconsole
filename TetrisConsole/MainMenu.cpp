#include "stdafx.h"
#include "MainMenu.h"


MainMenu::MainMenu(int width, int height)
{
	this->width = width;
	this->height = height;
	header = consoleEngine->CreateText(4, yOffset + 1, 4, tetrisHeader, 15);
	string str = "";
	for (auto &s : optionText) {
		str += s + "\n";
	}
	optionsText = consoleEngine->CreateText(17, yOffset + 8, 4, str, 15);
	arrow = consoleEngine->CreateText(12, yOffset + 8, 3, arrowStr, 15);
	versionText = consoleEngine->CreateText(36 - version.size(), 12, 4, version, 8);
	str = "";
	for (int h = 0; h < 11; h++) {
		for (int w = 0; w < 33; w++) {
			str += " ";
		}
		str += "\n";
	}
	backgroundBox = consoleEngine->CreateObject(3, yOffset, 2, str, '*', 0);
	str = "";
	for (int h = 0; h < 13; h++) {
		for (int w = 0; w < 35; w++) {
			str += "#";
		}
		str += "\n";
	}
	backgroundBoxBoarder = consoleEngine->CreateObject(2, yOffset - 1, 1, str, '*', 119);

	//Spawn stars
	for (size_t a = 0; a < 40; a++) {
		stars.push_back(new MenuStars(idCounter, consoleEngine->GetBoundsWidth(), consoleEngine->GetBoundsHeight()));
		idCounter++;
	}
}

MainMenu::~MainMenu()
{
	for (size_t star = 0; star < stars.size(); star++) {
		stars[star]->~MenuStars();
	}
	stars.clear();
	consoleEngine->DeleteText(optionsText);
	consoleEngine->DeleteText(arrow);
	consoleEngine->DeleteText(header);
	consoleEngine->DeleteObject(backgroundBox);
	consoleEngine->DeleteObject(backgroundBoxBoarder);
	consoleEngine->DeleteText(versionText);
}

int MainMenu::Update(float frametime)
{
	//Update stars
	int deadStarsCounter = 0;
	for (size_t star = 0; star < stars.size(); star++) {
		stars[star]->Update(frametime);
		if (stars[star]->GetDead()) {
			stars[star]->~MenuStars();
			deadStarsCounter++;
			stars.erase(stars.begin() + star);
			star--;
		}
	}

	for (int a = 0; a < deadStarsCounter; a++) {
		stars.push_back(new MenuStars(idCounter, consoleEngine->GetBoundsWidth(), consoleEngine->GetBoundsHeight()));
		idCounter++;
	}
	deadStarsCounter = 0;

	//Get and deal with input
	int in = consoleEngine->GetInput();
	if (in == 119) { //W
		if (currentSelected > 0) {
			currentSelected--;
			arrow->MoveUp(1);
		}
	}
	else if (in == 115) { //S
		if (currentSelected < (int)optionText.size() - 1) {
			currentSelected++;
			arrow->MoveDown(1);
		}
	}
	else if (in == 32) { //Space
		return currentSelected + 1;
	}

	return 0;
}