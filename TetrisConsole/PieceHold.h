#pragma once
#ifndef PIECEHOLD_H
#define PIECEHOLD_H

#include "ConsoleEngine.h"
#include "Piece.h"

using namespace consoleengine;
extern ConsoleEngine* consoleEngine;

class PieceHold
{
private:
	Text* header = NULL;
	Piece * piece = NULL;
	int holdX = 0;
	int holdY = 0;
	int objLocationX = 0;
	int objLocationY = 0;
	bool allowed = true;
	vector<Object*> lines;
	string text = "";
	Object* background = NULL;
public:
	PieceHold() {}
	PieceHold(int boardX, int boardY);
	~PieceHold() {}

	void PlaceInHold(Piece* piece);
	Piece* GetHoldPiece();
	bool IfEmpty();
	void SetAllowed(bool b);
	bool GetAllowed();
	void SetText(string str);
	void RemoveHold();
};

#endif // !PIECEHOLD_H