#include "stdafx.h"
#include "Piece.h"


Piece::Piece(int id, int boardX, int boardY, int boardXLocation, int boardYLocation)
{
	srand((unsigned int)time(NULL) + id);
	int rNum = rand() % PIECESAMOUNT;
	currentPiece = rNum;
	rotateNum = 0;
	this->worldX = boardXLocation + (boardX * 2);
	this->worldY = boardYLocation + boardY;
	this->boardX = boardX;
	this->boardY = boardY;
	//Populate squares
	CreateSquares(PIECES[currentPiece]->piece[rotateNum], PIECES[currentPiece]->colour);
}

Piece::Piece(int id, int boardX, int boardY, int boardXLocation, int boardYLocation, int rot, int piece)
{
	srand((unsigned int)time(NULL) + id);
	currentPiece = piece;
	rotateNum = rot;
	this->worldX = boardXLocation + (boardX * 2);
	this->worldY = boardYLocation + boardY;
	this->boardX = boardX;
	this->boardY = boardY;
	//Populate squares
	CreateSquares(PIECES[currentPiece]->piece[rotateNum], PIECES[currentPiece]->colour);
}

void Piece::CreateSquares(string str, int colour)
{
	int xPixelDefault = boardX;
	int xPixel = 0;
	int worldXPixel = 0;
	int yPixel = 0;
	int widthTemp = 0;
	height = 0;
	width = 0;
	bool skip = false;
	int widthCounter = 0;
	for (auto &c : str) {
		if (!skip) {
			if (c == '\n') {
				yPixel++;
				xPixel = 0;
				worldXPixel = 0;
				skip = false;
				height++;
				if (widthCounter > width) {
					width = widthCounter;
				}
				widthCounter = 0;
			}
			else if (c != TRANSPARENTCHAR) {
				squares.push_back(new Square(consoleEngine->CreateObject(
					worldX + worldXPixel, worldY + yPixel, 5, "[]", TRANSPARENTCHAR, colour),
					boardX + xPixel, boardY + yPixel)
				);
				xPixel++;
				worldXPixel += 2;
				skip = true;
				widthCounter++;
			}
			else if (c == TRANSPARENTCHAR) {
				xPixel++;
				worldXPixel += 2;
				skip = true;
			}
		}
		else {
			skip = false;
		}
	}
	if (widthCounter > width) {
		width = widthCounter;
	}
	height++;
}

void Piece::DeleteSquares()
{
	for (auto &s : squares) {
		if (s->piece != NULL) {
			s->boardX = 0;
			s->boardY = 0;
			consoleEngine->DeleteObject(s->piece);
		}
	}
	squares.clear();
}

void Piece::MoveLeft(int amount)
{
	boardX -= amount;
	worldX -= (amount * 2);
	for (auto &s : squares) {
		s->piece->MoveLeft(amount * 2);
		s->boardX -= amount;
	}
}

void Piece::MoveRight(int amount)
{
	boardX += amount;
	worldX += (amount * 2);
	for (auto &s : squares) {
		s->piece->MoveRight(amount * 2);
		s->boardX += amount;
	}
}

void Piece::MoveDown(int amount)
{
	boardY += amount;
	worldY += amount;
	for (auto &s : squares) {
		if (s->piece != NULL) {
			s->piece->SetPosition(s->piece->GetX(), s->piece->GetY() + amount);
			s->boardY += amount;
		}
	}
}

void Piece::MoveDown(int amount, int y)
{
	boardY += amount;
	worldY += amount;
	for (auto &s : squares) {
		if (s->piece != NULL && s->boardY < y) {
			s->piece->SetPosition(s->piece->GetX(), s->piece->GetY() + amount);
			s->boardY += amount;
		}
	}
}

int Piece::GetBoardX()
{
	return boardX;
}

int Piece::GetBoardY()
{
	return boardY;
}

int Piece::GetHeight()
{
	return height;
}

int Piece::GetWidth()
{
	return width;
}

void Piece::RotateLeft()
{
	rotateNum++;
	if (rotateNum == PIECES[currentPiece]->piece.size()) {
		rotateNum = 0;
	}
	DeleteSquares();
	CreateSquares(PIECES[currentPiece]->piece[rotateNum], PIECES[currentPiece]->colour);
	int temp = (boardX + width) - board->GetWidth();
	if (temp > 0) {
		MoveLeft(temp);
	}
}

void Piece::RotateRight()
{
	rotateNum--;
	if (rotateNum == -1) {
		rotateNum = PIECES[currentPiece]->piece.size() - 1;
	}
	DeleteSquares();
	CreateSquares(PIECES[currentPiece]->piece[rotateNum], PIECES[currentPiece]->colour);
	int temp = (boardX + width) - board->GetWidth();
	if (temp > 0) {
		MoveLeft(temp);
	}
}

int Piece::GetRotation()
{
	return rotateNum;
}

void Piece::SetWorldX(int x)
{
	int diff = x - worldX;
	worldX += diff;
	for (auto &s : squares) {
		if (s->piece != NULL) {
			s->piece->SetPosition(s->piece->GetX() + diff, s->piece->GetY());
		}
	}
}

void Piece::SetWorldY(int y)
{
	int diff = y - worldY;
	worldY += diff;
	for (auto &s : squares) {
		if (s->piece != NULL) {
			s->piece->SetPosition(s->piece->GetX(), s->piece->GetY() + diff);
		}
	}
}

int Piece::GetPiece()
{
	return currentPiece;
}