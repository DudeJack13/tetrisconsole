#include "stdafx.h"
#include "Board.h"


Board::Board()
{
	CreateBoard(DEFAULT_BOARD_WIDTH, DEFAULT_BOARD_HEIGHT);
}

Board::Board(int x, int y, int cellWidth, int cellHeight)
{
	boardX = x;
	boardY = y;
	CreateBoard(cellWidth, cellHeight);
}

void Board::Reset()
{
	for (auto &b : board) {
		b->taken = false;
		b->x = -1;
		b->y = -1;
	}
}

void Board::CreateBoard(int boardWidth, int boardHeight)
{
	consoleEngine->SetBackgroundColour(87);
	string bOut = "";
	for (int y = 0; y < consoleEngine->GetBoundsHeight(); y++) {
		for (int x = 0; x < consoleEngine->GetBoundsWidth(); x++) {
			bOut += " ";
		}
		bOut += "\n";
	}
	backgroundSquare = consoleEngine->CreateObject(0, 0, 0, bOut, '*', 119);

	this->boardHeight = boardHeight;
	this->boardWidth = boardWidth;
	//Create boarder
	string str = "";
	for (int x = 0; x < (boardWidth * 2) + 2; x++) {
		str += horizontalChar;
	}
	boarderLines.push_back(consoleEngine->CreateObject(boardX, boardY, 1, str, '*', 15));
	boarderLines.push_back(consoleEngine->CreateObject(boardX, (boardHeight + 1) + boardY, 1, str, '*', 15));
	str = "";
	for (int y = 0; y < boardHeight + 1; y++) {
		str += verticalChar;
		str += "\n";
	}
	boarderLines.push_back(consoleEngine->CreateObject(boardX, boardY, 1, str, '*', 15));
	boarderLines.push_back(consoleEngine->CreateObject(((boardWidth * 2) + 1) + boardX, boardY, 1, str, '*', 15));

	//Create squares inside of the board X AND Y NEED TO BE UN-OFFSET
	bOut = "";
	for (auto y = 0; y < boardHeight; y++) {
		for (auto x = 0; x < boardWidth; x++) {
			board.push_back(new BoardSquare(x,y));
			bOut += "  ";
		}
		bOut += "\n";
	}
	boardBackground = consoleEngine->CreateObject(boardX + 1, boardY + 1, 1, bOut, '*', 0);
	authorName = consoleEngine->CreateText(29, 28, 10, "Jack B", 120);
}

vector<int> Board::CheckLines()
{
	int counter = 0;
	vector<int> returnValue;
	for (int y = boardHeight - 1; y >= 0; y--) {
		for (int x = 0; x < boardWidth; x++) {
			if (GetTaken(x, y)) {
				counter++;
			}
		}
		if (counter == 11) {
			returnValue.push_back(y);
		}
		counter = 0;
	}
	return returnValue;
}

void Board::ConvertToBoardPosition(int & x, int & y)
{
	x = boardX + x + 1;
	y = boardY + y + 1;
}

void Board::ChangeTaken(int x, int y, bool taken)
{
	int _id = (boardWidth * y) + x;
	board[_id]->taken = taken;
}

bool Board::GetTaken(int x, int y)
{
	int id = (boardWidth * y) + x;
	return board[id]->taken;
}

bool Board::HitBottom(Object* obj)
{
	if ((obj->GetY() - 1) + obj->GetHeight() == boardHeight + 1) {
		return true;
	}
	return false;
}

int Board::GetX()
{
	return boardX;
}

int Board::GetY()
{
	return boardY;
}

int Board::GetWidth()
{
	return boardWidth;
}

int Board::GetHeight()
{
	return boardHeight;
}