# Windows Console Tetris #
Programmed by Jack Sylvester-Barnett

----------

VIDEO OF PROJECT: https://youtu.be/DA68awckijw 

Windows Console Tetris runs using C++ using a custom made Game-Engine for the Windows Console also created by me. [https://gitlab.com/DudeJack13/windows-console-engine](https://gitlab.com/DudeJack13/windows-console-engine "Windows Console Engine"). 

## The board ##
The Tetris console uses the engine to create block objects in the world and the position of objects on the board is tracked behind the scenes in a vector.

## The active Tetris piece ##
when a Tetris piece is spawned, it's then set as the current active piece (The piece that the player can interact with). When the active piece either hits the bottom of the board or if there is another Tetris piece (below the active piece), the active piece is moved to a vector list containing all the other pieces that have been placed. 

## Holding a piece ##
When the user wants to hold a piece, the object pointer is moved to a new variable and a new object is spawned. The object in the hold is removed from the board and moved to the hold position on the screen.

## Lines ##
When the active piece has been removed and placed into the placed pieces vector list, there is then a check on all the lines to see if each square in the lines is full. If so, the lines number is saved. If there are any line numbers, the line numbers are sorted and starting with the highest number, the lines above the number are all moved down by one. This processes repeats until all the numbers have been done.