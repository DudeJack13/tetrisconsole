#pragma once
#ifndef OJBJECT_H
#define OBJECT_H

#include "ObjectFunctions.h"

class Object : public ObjectFunctions
{
private:
	string charater;
	char transparentChar;
	bool rotated = false;
	vector <string> SplitCharacter();
public:
	Object();
	Object(int x, int y, int z, string str, int colour, char invisibleChar);
	~Object() {}

	//Rotation
	void RotatePos90();
	void RotateNeg90();
	bool BeenRotated();
	
	//Character functions
	string GetCharacter();
	void ChangeCharacter(string str);
	char TransparentChar();

	//Size Getters
	int GetHeight();
	int GetWidth();
};

#endif