//---------------------------------
//Windows Console Engine v1.131
//-Jack Sylvester-Barnett
//--------------------------------
#ifndef CONSOLEENGINE_H
#define CONSOLEENGINE_H
#pragma once

#include "EntityManager.h"
#include "DrawConsole.h"

#include <ctime>
#include <conio.h>

namespace consoleengine {
	
	class ConsoleEngine
	{
	private:
		bool running = true;
		int width = 0;
		int height = 0;
		clock_t lastTime;
		EntityManager* entityManager = NULL;
		DrawConsole* drawConsole = NULL;
		HWND hwnd = GetConsoleWindow();

		void SetUp();
		void Draw();
	public:
		ConsoleEngine();
		ConsoleEngine(int width, int height);
		
		//Object functions
		Object * CreateObject(int x, int y, int z, string character, char invisibleChar, int colour);
		void DeleteObject(Object* obj);

		//Text functions
		Text * CreateText(int x, int y, int z, string text, int colour);
		void DeleteText(Text* text);

		//Draw functions
		bool Running();
		void ExitEngine();

		//Engine functions
		void SetBounds(int width, int height);
		int GetBoundsWidth();
		int GetBoundsHeight();
		void SetBackgroundColour(int colour);
		//Only call once a loop
		float Timer();
		//If key pressed, key will be returned. If nothing, 0 is returned
		int GetInput();
	};
}

#endif // !CONSOLEENGINE_H

