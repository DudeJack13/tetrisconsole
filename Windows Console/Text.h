#pragma once
#ifndef TEXT_H
#define TEXT_H

#include "ObjectFunctions.h"

class Text : public ObjectFunctions
{
private:
	string text;

public:
	Text();
	Text(int x, int y, int z, string text, int colour);
	~Text() {}

	string GetText();
	void ChangeText(string text);
};

#endif // !TEXT_H