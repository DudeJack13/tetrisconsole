#pragma once
#ifndef OBJECTFUNCTIONS_H
#define OBJECTFUNCTIONS_H

#include <vector>
#include <string>

using namespace std;

class ObjectFunctions
{
protected:
	//Position
	int x;
	int xB;
	int y;
	int yB;
	int z;
	int colour;
	const int defaultColour = 15;
	bool visible = true;
	int width;
	int height = 1;
public:
	//Position getters
	int GetX();
	int GetY();
	int GetZ();
	int GetLastFrameX();
	int GetLastFrameY();

	void UpdateLastFrameLocation();

	//Position Function
	void MoveUp(int amount);
	void MoveDown(int amount);
	void MoveLeft(int amount);
	void MoveRight(int amount);
	void SetPosition(int x, int y);

	//Get colours
	int GetColour();
	void SetColour(int colour);

	//Other
	void Visible(bool visible);
	bool IfVisible();
};

#endif // !OBJECTFUNCTIONS_H