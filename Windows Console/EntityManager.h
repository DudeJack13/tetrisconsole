#ifndef ENTITYMANAGER_H
#define ENTITYMANAGER_H
#pragma once

#include "Object.h"
#include "Text.h"

class EntityManager
{
private:
	vector <Object*> objectList;
	vector <Text*> textList;
	vector <Object*> deletionList;
public:
	EntityManager();
	~EntityManager();

	//Object functions
	Object * CreateObject(int x, int y, int z, string character, char invisibleChar, int colour);
	void DeleteObject(Object* obj);

	//Text functions
	Text * CreateText(int x, int y, int z, string text, int colour);
	void DeleteText(Text* text);

	vector <Object*> GetObjectList();
	vector <Text*> GetTextList();
	bool IfDeletionEmpty();
	Object* GetDeletedObject();
	void RemoveDeletedObject();

	void Update();
};

#endif // ENTITYMANAGER_H